# Kafka Minimal Subscriber

## Getting started

### Use virtual environment
source bin/activate

### Export Python packages to requirements
pip freeze > requirements.txt

### Import Python packages from requirements
pip install -r requirements.txt
