#!/usr/bin/python

import yaml
from kafka import KafkaConsumer

KAFKA_TOPIC_NAME = "poc.precog.logging"
KAFKA_CONSUMER_GROUP_ID = "kafka-minimal-subscriber"

config = yaml.safe_load(open("localhost.yml"))

print("KAFKA_BOOTSTRAP_SERVERS =" + config["KAFKA_BOOTSTRAP_SERVERS"])
print("KAFKA_SECURITY_PROTOCOL =" + config["KAFKA_SECURITY_PROTOCOL"])
print("KAFKA_SASL_MECHANISM    =" + config["KAFKA_SASL_MECHANISM"])
print("KAFKA_SASL_USERNAME     =" + config["KAFKA_SASL_USERNAME"])
print("KAFKA_SASL_PASSWORD     =" + config["KAFKA_SASL_PASSWORD"])


consumer = KafkaConsumer(
    KAFKA_TOPIC_NAME,
    bootstrap_servers=config["KAFKA_BOOTSTRAP_SERVERS"],
    security_protocol=config["KAFKA_SECURITY_PROTOCOL"],
    sasl_mechanism=config["KAFKA_SASL_MECHANISM"],
    sasl_plain_username=config["KAFKA_SASL_USERNAME"],
    sasl_plain_password=config["KAFKA_SASL_PASSWORD"],
    auto_offset_reset="earliest",  # errors: ‘earliest’ will move to the oldest available message, ‘latest’ will move to the most recent. 
    enable_auto_commit=True,
    group_id=KAFKA_CONSUMER_GROUP_ID,
)

for message in consumer:
    if message:
        value = message.value.decode("utf-8")
        print("{}\n---".format(value))
